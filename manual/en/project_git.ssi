:B~ Git repositories

1~git-repositories Git repositories

The list of all the available repositories of the ${project} can be found at http://http://anonscm.debian.org/cgit/debian-live/. The project's git URLs have the form: #{protocol://http://anonscm.debian.org/git/debian-live/repository}#. Thus, in order to clone live-manual read-only, launch:

code{

 $ git clone git://http://anonscm.debian.org/git/debian-live/live-manual.git

}code

Or,

code{

 $ git clone https://http://anonscm.debian.org/git/debian-live/live-manual.git

}code

Or,

code{

 $ git clone http://http://anonscm.debian.org/git/debian-live/live-manual.git

}code

The cloning addresses with write permission have the form: #{ssh://git.debian.org/git/debian-live/repository}#.

So, again, to clone live-manual over ssh you must type:

code{

 $ git clone ssh://git.debian.org/git/debian-live/live-manual.git

}code

The git tree is made up of several different branches. The *{debian}* and the *{debian-next}* branches are particularly noteworthy because they contain the actual work that will eventually be included in each new release.

After cloning any of the existing repositories, you will be on the *{debian}* branch. This is appropriate to take a look at the state of the project's latest release but before starting work it is crucial to switch to the *{debian-next}* branch. To do so:

code{

 $ git checkout debian-next

}code

The *{debian-next}* branch, which is not always fast-forward, is where all the changes are committed first before being merged into the *{debian}* branch. To make an analogy, it is like a testing ground. If you are working on this branch and need to pull, you will have to do a #{git pull --rebase}# so that your local modifications are staged while pulling from the server and then your changes will be put on top of it all.

2~ Handling multiple repositories

If you intend to clone several of the live systems repositories and want to switch to the *{debian-next}* branch right away to check the latest code, write a patch or contribute with a translation you ought to know that the git server provides a #{mrconfig}# file to ease the handling of multiple repositories. In order to use it you need to install the /{mr}/ package and after that, launch:

code{

 $  mr bootstrap http://debian-live.alioth.debian.org/other/mr/mrconfig

}code

This command will automatically clone and checkout to the *{debian-next}* branch the development repositories of the Debian packages produced by the project. These include, among others, the live-images repository, which contains the configurations used for the prebuilt images that the project publishes for general use. For more information on how to use this repository, see {Clone a configuration published via Git}#clone-configuration-via-git
