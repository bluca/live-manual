:B~ Contributing to the project

1~contributing-to-project Contributing to the project

When submitting a contribution, please clearly identify its copyright holder and include any applicable licensing statement. Note that to be accepted, the contribution must be licensed under the same license as the rest of the documents, namely, GPL version 3 or later.

Contributions to the project, such as translations and patches, are greatly welcome. Anyone can directly commit to the repositories, however, we ask you to send bigger changes to the mailing list to discuss them first. See the section {Contact}#contact for more information.

The ${project} uses Git as version control system and source code management. As explained in {Git repositories}#git-repositories there are two main development branches: *{debian}* and *{debian-next}*. Everybody can commit to the debian-next branches of the live-boot, live-build, live-config, live-images, live-manual and live-tools repositories.

However, there are certain restrictions. The server will reject:

_* Non fast-forward pushes.

_* Merge commits.

_* Adding or removing tags or branches.

Even though all commits might be revised, we ask you to use your common sense and make good commits with good commit messages.

_* Write commit messages that consist of complete, meaningful sentences in English, starting with a capital letter and ending with a full stop. Usually, these will start with the form "Fixing/Adding/Removing/Correcting/Translating/...".

_* Write good commit messages. The first line must be an accurate summary of the contents of the commit which will be included in the changelog. If you need to make some further explanations, write them below leaving a blank line after the first one and then another blank line after each paragraph. Lines of paragraphs should not exceed 80 characters in length.

_* Commit atomically, this is to say, do not mix unrelated things in the same commit. Make one different commit for each change you make.

2~ Making changes

In order to push to the repositories, you must follow the following procedure. Here we use live-manual as an example so replace it with the name of the repository you want to work with. For detailed information on how to edit live-manual see {Contributing to this document}#how-to-contribute.

_* Fetch the public commit key:

code{

 $ mkdir -p ~/.ssh/keys
 $ wget http://debian-live.alioth.debian.org/other/keys/git@debian-live.alioth.debian.org -O ~/.ssh/keys/git@debian-live.alioth.debian.org
 $ wget http://debian-live.alioth.debian.org/other/keys/git@debian-live.alioth.debian.org.pub -O ~/.ssh/keys/git@debian-live.alioth.debian.org.pub
 $ chmod 0600 ~/.ssh/keys/git@debian-live.alioth.debian.org*

}code

_* Add the following section to your openssh-client config:

code{

 $ cat >> ~/.ssh/config << EOF
 Host debian-live.alioth.debian.org
     Hostname debian-live.alioth.debian.org
     User git
     IdentitiesOnly yes
     IdentityFile ~/.ssh/keys/git@debian-live.alioth.debian.org
 EOF

}code

_* Check out a clone of live-manual through ssh:

code{

 $ git clone ssh://git.debian.org/git/debian-live/live-manual.git
 $ cd live-manual && git checkout debian-next

}code

_* Make sure you have Git author and email set:

code{

  $ git config user.name "John Doe"
  $ git config user.email john@example.org

}code

*{Important:}* Remember that you should commit any changes on the *{debian-next}* branch.

_* Make your changes. In this example you would first write a new section dealing with applying patches and then prepare to commit adding the files and writing your commit message like this:

code{

 $ git commit -a -m "Adding a section on applying patches."

}code

_* Push the commit to the server:

code{

 $ git push

}code

2~translation-of-manpages Translation of man pages

You can also contribute to the project working on the translation of the man pages for the different live-* packages that the project maintains. The procedure is different depending on whether you are starting a translation from scratch or continue working on an already existing one:

_* Working on an already existing translation

If you want to maintain the translation of an already existing language you have to make your changes to your #{manpages/po/${LANGUAGE}/*.po}# file or files and then run #{make rebuild}# from inside the #{manpages/}# directory. This will update the actual man pages in #{manpages/${LANGUAGE}/*}#

_* Starting a new translation from scratch

In order to add a new translation of any of the project's man pages you have to follow a similar procedure. It could be summarized as follows:

_2* Open the #{manpages/pot/}# file or files in your favourite editor, such as /{poedit}/, and save it as a .po file in #{manpages/po/${LANGUAGE}/}#. (You will have to create your #{${LANGUAGE}/}# directory).

_2* Run #{make rebuild}# from inside the #{manpages/}# directory to create the #{manpages/${LANGUAGE}/}# files which will contain the actual man pages.

Remember that you will have to add all the directories and files, then make the commit and finally push to the git server.

